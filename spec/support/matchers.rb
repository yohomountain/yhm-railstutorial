# Check for an <a> tag with the exact text and optional href
# Same param syntax as has_link, but it only matches the link's TEXT, not id,
# label, etc., and it doesn't match substrings.
def has_exact_title?(locator, options={})

  # Subtract :href from array of options.keys, raise error if any options remain
  raise ArgumentError.new "has_exact_title only supports 'text' as an option" unless
    (options.keys - [:text]).empty?
  test = options[:text].to_s
  #xpath = (/^#{Regexp.escape(test)}$/)
  # pass original options has so test results will show href if present
  has_xpath?(test, options)

end